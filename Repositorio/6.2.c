#include <stdio.h>
int main(int argc, char const *argv[]) {
  float radio;
  printf("Introduce el radio: ");
  scanf("%f",&radio );
  while (radio<=0) {
    printf("ERROR: Nota el radio debe ser >= 0\n" );
    printf("Introduce el radio: ");
    scanf("%f",&radio );
  }
  if (radio>0) {
    printf("El area es: %.2f\n",(radio*radio)*3.14);
  }
  return 0;
}
