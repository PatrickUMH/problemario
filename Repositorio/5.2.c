#include <stdio.h>
int main(int argc, char const *argv[]) {
  char letra;
  printf("Escribe una letra: " );
  scanf("%c",&letra );
  if (letra == 'a' || letra == 'A' ||
      letra == 'e' || letra == 'E' ||
      letra == 'i' || letra == 'I' ||
      letra == 'o' || letra == 'O' ||
      letra == 'a' || letra == 'U' ) {
    printf("%c es una vocal\n",letra );
  } else {
    printf("%c no es una vocal\n",letra );
  }
  return 0;
}
