#include <stdio.h>
int main(int argc, char const *argv[]) {
  float calif;
  printf("Introduce la calificacion: ");
  scanf("%f",&calif );
  while (calif<0 || calif>10) {
    printf("ERROR: Nota incorrecta, debe ser >= 0 y <= 10\n" );
    printf("Introduce la calificacion: ");
    scanf("%f",&calif );
  }
  if (calif<6) {
    printf("REPROBADO\n");
  } else {
    printf("APROBADO\n");
  }
  return 0;
}
