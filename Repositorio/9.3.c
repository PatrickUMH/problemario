#include <stdio.h>
int main(int argc, char const *argv[]) {
    int opcion, n1, n2;
    printf( "MENU CALCULADORA\n" );
    printf( "1) Sumar\n");
    printf( "2) Restar\n");
    printf( "3) Multiplicacion\n");
    printf( "4) Division\n");
    printf( "5) Modulo\n");
    printf( "6) Salir\n");
    printf("Seleccion: " );
    scanf("%d",&opcion );

    switch ( opcion ){
        case 1:
        printf( "Introduzca primer valor: " );
        scanf( "%d", &n1);
        printf( "Introduzca segundo valor: " );
        scanf( "%d", &n2);
        printf( " %d + %d = %d\n", n1, n2, n1 + n2 );
        break;

        case 2:
        printf( "Introduzca primer valor: " );
        scanf( "%d", &n1);
        printf( "Introduzca segundo valor: " );
        scanf( "%d", &n2);
        printf( "%d - %d = %d\n", n1, n2, n1 - n2 );
        break;

        case 3:
        printf("Introduzca el primer valor: ");
        scanf( "%d", &n1);
        printf("Introduzca el segundo valor: ");
        scanf( "%d", &n2);
        printf( "%d * %d = %d\n", n1, n2, n1 * n2 );
        break;

        case 4:
        printf("Introduzca el primer valor: ");
        scanf( "%d", &n1);
        printf("Introduzca el segundo valor: ");
        scanf( "%d", &n2);
        if ( n2 != 0 ){
        printf( "%d div %d = %d )\n", n1, n2, n1 / n2);
        }else{
        printf( "ERROR: No se puede dividir entre cero.\n" );
        }
        break;
        case 5:
        printf("Introduzca el primer valor: ");
        scanf( "%d", &n1);
        printf("Introduzca el segundo valor: ");
        scanf( "%d", &n2);
        printf("%d Modulo %d = %d\n",n1,n2,n1%n2 );
        break;
        default:
        printf("ERROR %d no es una opcion del MENU\n",opcion );
    }

    return 0;
}
